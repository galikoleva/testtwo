public class Customer {
    private final String EIK;
    private final String Name;
    private final String ManagerName;
    private final String ManagerPhone;

    public Customer(String eik, String name, String managerName, String managerPhone) {
        this.EIK = eik;
        this.ManagerName = managerName;
        this.ManagerPhone = managerPhone;
        this.Name = name;
    }

    public String getEIK() {
        return this.EIK;
    }

    public String getName() {
        return this.Name;
    }

    public String getManagerName() {
        return this.ManagerName;
    }

    public String getManagerPhone() {
        return this.ManagerPhone;
    }
}
