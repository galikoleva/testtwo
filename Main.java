import DataBase.Table.Table;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Customer restaurant = new Customer("1234","name","manager","8888");
        Scanner scanner = new Scanner(System.in);

        System.out.println("въведете кода на сервитьора");
        String idnumber = scanner.nextLine(); //записваме стойността въведена от клавиатурата във idnumber

        Waiter.checkWaiter(idnumber);
        //Waiter assignedWaiterName = Waiter.Names(idnumber);

        System.out.println("Въведете номер на маса: ");
        int tablenum = scanner.nextInt();

        Table.checkTable(tablenum);

        System.out.println("End of code");
    }
}
